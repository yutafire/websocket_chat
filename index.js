const express = require('express');
const socket = require('socket.io');

let app = express();
let server = app.listen(3000, function(){
    console.log('listening to requests on port 3000');
});

app.use(express.static('public'));

// socket setup

let io = socket(server);

// listening to start of socket connection
// コールバックがコネクション確立時に実行される
// ページをリロードするたびにsocket.idが変化する
// →毎回コネクションが確立されて異なる識別子を持つ
io.on('connection', function(socket){
    console.log('made socket connection', socket.id)

    // socketという変数にはデータをサーバに送ってきた特定のクライアントと結びついている。
    // particular socket
    // data is the data sent by socket.emit
    // 'chat'というフレームが送られると動く
    // emit('chat', {})で{}がサーバに送られる
    socket.on('chat', function (data) {
        // io.sockets＝コネクション確立しているすべてのsocket
        // emitですべてのsocketにdataを送る
        console.log(data);

        // socketsという変数には接続されているすべてのクライアントが結びついている
        // すべてのクライアントにdataを送信する。
        io.sockets.emit('chat', data);
    });

    socket.on('typing', function (data) {
        // broadcastはsocket(＝フレームを送ってきたクライアント)以外のsocketにつながっているクライアントに
        // フレームを送れる。
        socket.broadcast.emit('typing', data);
    });
});

